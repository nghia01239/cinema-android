
package com.demo.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Seat_ implements Serializable {

    @SerializedName("seat_id")
    @Expose
    private String seatId;
    @SerializedName("srat_type")
    @Expose
    private String sratType;
    @SerializedName("checked")
    @Expose
    private boolean checked;

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @SerializedName("room")
    @Expose
    private Room____ room;
    @SerializedName("row")
    @Expose
    private String row;
    @SerializedName("number")
    @Expose
    private String number;

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }

    public String getSratType() {
        return sratType;
    }

    public void setSratType(String sratType) {
        this.sratType = sratType;
    }

    public Room____ getRoom() {
        return room;
    }

    public void setRoom(Room____ room) {
        this.room = room;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
