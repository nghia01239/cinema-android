package com.demo.cinemaapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.demo.api.APIClient;
import com.demo.api.UserAPI;
import com.demo.entities.Schedule;
import com.demo.entities.Seat;
import com.demo.entities.User;
import com.demo.helper.DatabaseHelper;
import com.demo.models.BCrypt;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText edUsername, edPass;
    private Button btnLogin, btnRegister;
    private User user;
    private List<User> users;
    private Seat seat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        loadData();

    }

    private void initView(){
        edUsername = findViewById(R.id.editTextUsername);
        edPass = findViewById(R.id.editTextPassword);
        btnLogin = findViewById(R.id.buttonSigin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLogin_onClick(v);
            }
        });
        btnRegister = findViewById(R.id.buttonRegister);

    }
    private void loadData(){

        UserAPI userAPI = APIClient.getClient().create(UserAPI.class);
        userAPI.findAll().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                try{
                    users = response.body();

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }

    public void btnLogin_onClick(View v) {
        Intent intent = getIntent();
        String booking;
        seat = (Seat) intent.getSerializableExtra("seat");
        booking = (String) intent.getSerializableExtra("booking");
        Schedule schedule = (Schedule) intent.getSerializableExtra("schedule");
        for (User u : users){
            String username = edUsername.getText().toString().trim();
            String pass = edPass.getText().toString().trim();

            if(u.getUsername().equalsIgnoreCase(username) && BCrypt.checkpw(pass, u.getPassword()))
            {
                User user1 = new User();
                user1.setId(u.getId());
                user1.setUsername(u.getUsername());
                user1.setPassword(u.getPassword());
                user1.setAvatar(u.getAvatar());
                user1.setFullname(u.getFullname());
                user1.setPoint(u.getPoint());user1.setCity(u.getCity());user1.setPhone(u.getPhone());
                user1.setEmail(u.getEmail());user1.setGender(u.getGender());user1.setBirthday(u.getBirthday());
                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                databaseHelper.creater(user1);
                user = databaseHelper.findAccount();
                if(booking.equalsIgnoreCase("booking")){
                    Intent intent1 = new Intent(LoginActivity.this, BookingActivity.class);
                    intent1.putExtra("user", user);
                    intent1.putExtra("schedule", schedule);
                    intent1.putExtra("seat", seat);
                    startActivity(intent1);
                }else if(booking.equalsIgnoreCase("login")){
                    Intent intent2 = new Intent(LoginActivity.this, MainActivity.class);

                    startActivity(intent2);
                }

            }
        }

    }

}
