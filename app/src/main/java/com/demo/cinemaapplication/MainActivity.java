package com.demo.cinemaapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.demo.adapters.MovieAdapter;
import com.demo.api.APIClient;
import com.demo.api.MovieAPI;
import com.demo.entities.Movie;
import com.demo.entities.User;
import com.demo.helper.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ListView lsVMovie;
    private ImageButton btnMic;

    private User user;
    private List<Movie> movies;

    private String hear;
    private TextToSpeech textToSpeech;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initview();
        loadData();

    }

    private void initview(){
        lsVMovie = findViewById(R.id.listViewMovie);
        btnMic = findViewById(R.id.buttonMic);
        btnMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick(v);
            }
        });
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

            }
        });
        lsVMovie.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lsVMovie_onItemClick(parent, view, position, id);
            }
        });
    }

    private void loadData(){

        /*User user1 = new User();
        user1.setId("0");
        user1.setUsername("anonymous");
        user1.setPassword("null");
        user1.setAvatar(String.valueOf(R.drawable.anonymous));
        user1.setFullname("Anonymous");
        user1.setPoint("null");user1.setCity("null");user1.setPhone("null");
        user1.setEmail("null");user1.setGender("null");user1.setBirthday("null");
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        databaseHelper.creater(user1);*/
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        if (databaseHelper.findAccount() != null) {
            User user1 = databaseHelper.findAccount();
            Toast.makeText(getApplicationContext(), "" + user1.getUsername(), Toast.LENGTH_SHORT).show();
        }else if(databaseHelper.findById("0") != null){
            User user1 = databaseHelper.findById("0");
            Toast.makeText(getApplicationContext(), "" + user1.getId(), Toast.LENGTH_SHORT).show();
        }

        MovieAPI movieAPI = APIClient.getClient().create(MovieAPI.class);
        movieAPI.findAll().enqueue(new Callback<List<Movie>>() {
            @Override
            public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                try{
                    movies = response.body();
                    MovieAdapter movieAdapter = new MovieAdapter(getApplicationContext(), R.layout.list_movie_layout,movies);
                    lsVMovie.setAdapter(movieAdapter);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                    Toast.makeText(getApplicationContext(), e.getMessage() + "1", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Movie>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void lsVMovie_onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Movie movie = (Movie) parent.getItemAtPosition(position);

        Intent intent1 = new Intent(MainActivity.this, MovieDetailActivity.class);
        intent1.putExtra("movie", movie);
        startActivity(intent1);

    }



    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());

        MenuItem menuItem = menu.findItem(R.id.menuLogin);
        if (databaseHelper.findAccount() != null) {
            user = databaseHelper.findAccount();
            menuItem.setTitle(R.string.logout);
        }else {
            user = databaseHelper.findById("0");
            menuItem.setTitle(R.string.login);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.menuLsCinema){
            menulsCinema(item);
        }else if(item.getItemId() == R.id.menuMovieSche){
            menuMovieSche(item);
        }else if(item.getItemId() == R.id.menuLogin){
            menuLogin(item);
        }else if(item.getItemId() == R.id.menuaccount){
            menuAccount(item);
        }

        return super.onOptionsItemSelected(item);
    }

    private void menulsCinema(MenuItem item){

    }
    private void menuMovieSche(MenuItem item){

    }

    private void menuLogin(MenuItem item){
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        if (databaseHelper.findAccount() != null) {
            User user = databaseHelper.findAccount();
            databaseHelper.delete(user.getId());
            loadData();
        }else {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.putExtra("booking", "login");
            startActivity(intent);
        }

    }
    private void menuAccount(MenuItem item){
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        if(databaseHelper.findAccount() != null) {
            user = databaseHelper.findAccount();
            Intent intent = new Intent(MainActivity.this, AccountActivity.class);
            intent.putExtra("user", user);
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(), "null Account ", Toast.LENGTH_SHORT).show();
        }
    }

    //speed
    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput() {
        Intent intent= new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hi speak something");

        //start intent
        try {
            startActivityForResult(intent, 10);

        }catch (Exception e){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=APP_PACKAGE_NAME"));
            startActivity(browserIntent);
            Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            System.out.println(e.getMessage());
        }

    }
    public void onClick(View v) {

    }
    //receive voice inpit and handle it
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10: {
                if (resultCode == 10 && data != null) {
                    //get text array from voice intent
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    hear=result.get(0);
                }
                break;
            }

        }
        String toSpeak = hear;
        String uttreanceId = UUID.randomUUID().toString();

        if(toSpeak.equalsIgnoreCase("hello")) {
            Toast.makeText(getApplicationContext(), uttreanceId, Toast.LENGTH_SHORT).show();
            textToSpeech.speak("hello can i help you", TextToSpeech.QUEUE_FLUSH, null, uttreanceId);
        }else if(toSpeak.contains("handsome")){
            textToSpeech.speak("ofcouse sir", TextToSpeech.QUEUE_FLUSH, null, uttreanceId);
        }
    }

}
