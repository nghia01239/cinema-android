package com.demo.cinemaapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.demo.entities.User;
import com.demo.helper.DatabaseHelper;

import java.io.InputStream;

public class AccountActivity extends AppCompatActivity {

    private TextView txtAcName, txtAcPhone, txtAcEmail, txtAcAddress, txtAcPoint;
    private Button btnEditAc, btnTranHis;
    private ImageView imgVAccount;

    private User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        initview();
        loadData();
    }

    private void initview(){
        txtAcName = findViewById(R.id.textViewAcFullname);
        txtAcEmail = findViewById(R.id.textViewAcEmail);
        txtAcAddress = findViewById(R.id.textViewAcAddress);
        txtAcPhone = findViewById(R.id.textViewAcPhone);
        txtAcPoint= findViewById(R.id.textViewAcPoint);
        imgVAccount = findViewById(R.id.imageViewAccount);
        btnEditAc = findViewById(R.id.buttonEditAc);
        btnTranHis = findViewById(R.id.buttonTranHis);

        btnTranHis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                His_onClick(v);
            }
        });
        btnEditAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit_onClick(v);
            }
        });
    }

    private void loadData(){
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        user = databaseHelper.findAccount();
        txtAcName.setText(user.getFullname());
        txtAcEmail.setText(user.getEmail());
        txtAcAddress.setText(user.getCity());
        txtAcPhone.setText(user.getPhone());
        txtAcPoint.setText(user.getPoint());
        new ImageRequesAsk(imgVAccount).execute("http://192.168.56.1:9567/resources/uploads/images/"+  user.getAvatar());

    }

    //hàm loadImage từ data
    private class ImageRequesAsk extends AsyncTask<String, Void, Bitmap>{

        private ImageView imageViewPhoto;

        public ImageRequesAsk(ImageView imageViewPhoto){
            this.imageViewPhoto = imageViewPhoto;
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                InputStream inputStream = new java.net.URL(params[0]).openStream();
                return BitmapFactory.decodeStream(inputStream);
            }catch (Exception e){
                return  null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageViewPhoto.setImageBitmap(bitmap);
        }
    }


    public void Edit_onClick(View v) {

    }

    public void His_onClick(View v) {
        Intent intent = new Intent(AccountActivity.this, TranHisActivity.class);
        startActivity(intent);
    }
}
