package com.demo.cinemaapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.demo.adapters.ScheduleAdapter;
import com.demo.api.APIClient;
import com.demo.api.SchemaAPI;
import com.demo.entities.Movie;
import com.demo.entities.Schedule;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailActivity extends AppCompatActivity {

    private TextView txtNameMovieDetail, txtMovieLenght, txtMovieDes;
    private ImageView imgMoviePostDetail;
    private Button btnTrailer;
    private ListView lsVSchedule;

    private Movie movie;
    private List<Schedule> schedules;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        initView();
        loadData();
    }

    private void initView(){
        txtNameMovieDetail = findViewById(R.id.textViewMovieNameDetail);
        txtMovieLenght = findViewById(R.id.textViewMovieLenght);
        txtMovieDes = findViewById(R.id.textViewMovieDes);
        imgMoviePostDetail = findViewById(R.id.imageViewMoviePostDetail);
        lsVSchedule = findViewById(R.id.ListViewSchedule);
        btnTrailer = findViewById(R.id.buttonTrailer);

        lsVSchedule.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List_schedule_onItemClick(parent, view, position, id);
            }
        });

        btnTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MovieDetailActivity.this, TrailerActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadData(){
        Intent intent = getIntent();
        movie = (Movie) intent.getSerializableExtra("movie");
        txtNameMovieDetail.setText(movie.getMovieName());
        txtMovieLenght.setText(movie.getMovieLength());
        txtMovieDes.setText(movie.getMovieDescription());
        Date currentTime = Calendar.getInstance().getTime();
        new ImageRequesAsk(imgMoviePostDetail).execute("http://192.168.56.1:9567/resources/uploads/images/"+ movie.getMoviePoster());
        SchemaAPI schemaAPI = APIClient.getClient().create(SchemaAPI.class);
        schemaAPI.findByMovie(Integer.parseInt(movie.getMovieId()),0, currentTime.toString()).enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                try{
                    schedules = response.body();
                    ScheduleAdapter scheduleAdapter = new ScheduleAdapter(getApplicationContext(), R.layout.schedule_list_layout, schedules);
                    lsVSchedule.setAdapter(scheduleAdapter);

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    public void List_schedule_onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Schedule schedule = (Schedule) parent.getItemAtPosition(position);
            Intent intent1 = new Intent(MovieDetailActivity.this, SeatActivity.class);
            intent1.putExtra("schedule", schedule);

            startActivity(intent1);

    }


    private class ImageRequesAsk extends AsyncTask<String, Void, Bitmap> {

        private ImageView imageViewPhoto;

        public ImageRequesAsk(ImageView imageViewPhoto){
            this.imageViewPhoto = imageViewPhoto;
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                InputStream inputStream = new java.net.URL(params[0]).openStream();
                return BitmapFactory.decodeStream(inputStream);
            }catch (Exception e){
                return  null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageViewPhoto.setImageBitmap(bitmap);
        }
    }
}
