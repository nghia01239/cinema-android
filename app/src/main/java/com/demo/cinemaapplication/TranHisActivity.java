package com.demo.cinemaapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.demo.adapters.TranAdapter;
import com.demo.api.APIClient;
import com.demo.api.BookingAPI;
import com.demo.entities.Booking;
import com.demo.entities.User;
import com.demo.helper.DatabaseHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TranHisActivity extends AppCompatActivity {

    private ListView listView;

    private List<Booking> bookings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tran_his);
        initView();
        loadData();
    }

    private void initView(){
        listView = findViewById(R.id.listViewTranDetail);

    }

    private void loadData(){
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        User user = new User();
        user = databaseHelper.findAccount();
        BookingAPI bookingAPI = APIClient.getClient().create(BookingAPI.class);
        bookingAPI.findByUser(Integer.parseInt(user.getId())).enqueue(new Callback<List<Booking>>() {
            @Override
            public void onResponse(Call<List<Booking>> call, Response<List<Booking>> response) {
                try{
                    bookings = response.body();
                    TranAdapter tranAdapter = new TranAdapter(getApplicationContext(), R.layout.trans_his_layout, bookings);
                    listView.setAdapter(tranAdapter);


                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Booking>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
