package com.demo.cinemaapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.adapters.SeatAdapter;
import com.demo.api.APIClient;
import com.demo.api.BookingAPI;
import com.demo.api.SeatsAPI;
import com.demo.entities.Booking;
import com.demo.entities.Curren_user;
import com.demo.entities.Schedule;
import com.demo.entities.Seat;
import com.demo.entities.User;
import com.demo.helper.DatabaseHelper;
import com.demo.models.UserModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeatActivity extends AppCompatActivity {

    private TextView txtBNameMovie, txtBNameCinema, txtBRoom;
    private GridView gridViewChair;

    private List<Seat> seats;
    private Schedule schedule;
    private List<Booking> bookings;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        initView();
        loadData();
    }

    private void initView(){
        txtBNameMovie = findViewById(R.id.textViewBookMovieName);
        txtBNameCinema = findViewById(R.id.textViewBookCinemaName);
        txtBRoom = findViewById(R.id.textViewBookCinemaRoom);
        gridViewChair = findViewById(R.id.GribViewChair);

        gridViewChair.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GrV_onItemClick(parent, view, position, id);
            }
        });

    }
    private void loadData(){
        Intent intent = getIntent();
        schedule = (Schedule) intent.getSerializableExtra("schedule");
        txtBNameMovie.setText(schedule.getMovie().getMovieName());
        txtBRoom.setText(schedule.getRoom().getRoomName());
        txtBNameCinema.setText(schedule.getRoom().getCinema().getCinemaName());

        //Gọi booking của schedule này
        BookingAPI bookingAPI = APIClient.getClient().create(BookingAPI.class);
        bookingAPI.findBySchedule(Integer.parseInt(schedule.getScheduleId())).enqueue(new Callback<List<Booking>>() {
            @Override
            public void onResponse(Call<List<Booking>> call, Response<List<Booking>> response) {
                try {
                    if(response.body() != null){
                        bookings = response.body();
                    }

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Booking>> call, Throwable t) {

            }
        });
        //So sánh chỗ ngồi schedule này đã dc book chưa
        SeatsAPI seatsAPI = APIClient.getClient().create(SeatsAPI.class);
        seatsAPI.findByRoom(Integer.parseInt(schedule.getRoom().getRoomId())).enqueue(new Callback<List<Seat>>() {
            @Override
            public void onResponse(Call<List<Seat>> call, Response<List<Seat>> response) {
                try {
                    seats = response.body();
                    if (bookings.size() != 0){
                    for (Booking b : bookings){
                        for (Seat s : seats){
                            if(b.getSeat().getSeatId().equalsIgnoreCase(s.getSeatId().trim())){
                                if (b.getSeatStatus().equalsIgnoreCase("1")){
                                    s.setChecked(false);
                                }else {
                                    s.setChecked(true);
                                }
                            }
                        }
                    }}
                    SeatAdapter seatAdapter = new SeatAdapter(getApplicationContext(), R.layout.gridview_seat_layout, seats);
                    gridViewChair.setAdapter(seatAdapter);

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Seat>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    public void GrV_onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Seat seat = (Seat) parent.getItemAtPosition(position);
        if(seat.getChecked()) {

            DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
            user = databaseHelper.findAccount();
            //nếu mà đã đăng nhập user
            if (user != null) {
                Intent intent = new Intent(SeatActivity.this, BookingActivity.class);
                intent.putExtra("schedule", schedule);
                intent.putExtra("seat", seat);
                startActivity(intent);
            }
            //nếu chưa đăng nhập
            else {
                Toast.makeText(getApplicationContext(), "please login first!!!", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SeatActivity.this, LoginActivity.class);
                intent.putExtra("booking", "booking");
                intent.putExtra("seat", seat);
                intent.putExtra("schedule", schedule);
                startActivity(intent);
            }
        }else {
            Toast.makeText(getApplicationContext(), "This seat is not avalable !!", Toast.LENGTH_SHORT).show();
        }

    }
}
