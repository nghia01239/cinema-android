package com.demo.cinemaapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.api.APIClient;
import com.demo.api.BookingAPI;
import com.demo.entities.Booking;
import com.demo.entities.Schedule;
import com.demo.entities.Seat;
import com.demo.entities.User;
import com.demo.helper.DatabaseHelper;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingActivity extends AppCompatActivity {

    private TextView txtBName, txtBRoom, txtBTime, txtBPrice, txtBUsName, txtBSeat;
    private Button btnok;
    private User user;
    private Seat seat;
    private Schedule schedule;
    private Booking booking = new Booking();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking2);
        initView();
        loadData();
    }

    private void initView(){
        txtBName = findViewById(R.id.textViewBookingNameMovie);
        txtBRoom = findViewById(R.id.textViewBookingRoom);
        txtBPrice = findViewById(R.id.textViewBookingPrice);
        txtBTime = findViewById(R.id.textViewBookingTime);
        txtBUsName = findViewById(R.id.textViewUserFullname);
        btnok = findViewById(R.id.button);
        txtBSeat = findViewById(R.id.textViewBookingSeat);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnBook_onClick(v);
            }
        });
    }

    private void loadData(){
        Intent intent = getIntent();
        seat = (Seat) intent.getSerializableExtra("seat");
        schedule = (Schedule) intent.getSerializableExtra("schedule");
        txtBName.setText(schedule.getMovie().getMovieName().toString());
        txtBRoom.setText(schedule.getRoom().getRoomName() + ": ");
        txtBTime.setText(schedule.getScheduleStart().toString());
        String col = "";
        if(seat.getRow().equalsIgnoreCase("Row 1")){
            col = "A";
        }else if(seat.getRow().equalsIgnoreCase("Row 2")){
            col = "B";
        }else if(seat.getRow().equalsIgnoreCase("Row 3")) {
            col = "C";
        }else if(seat.getRow().equalsIgnoreCase("Row 4")){
            col = "D";
        }else if(seat.getRow().equalsIgnoreCase("Row 5")){
            col = "E";
        }
        txtBSeat.setText(col + " - " + seat.getNumber());

        int price = 0;
        String h = schedule.getScheduleStart().substring(0,2);
        if( Integer.parseInt(h) > 16 ){
            price =60000;
        }else {
            price = 55000;
        }
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        user = databaseHelper.findAccount();
        txtBPrice.setText(String.valueOf(price));
        txtBUsName.setText(user.getFullname());

        booking.setPrice(String.valueOf(price));
        booking.setSchedule(schedule);
        booking.setUser(user);
        booking.setSeat(seat);
        booking.setSeatStatus("1");
        booking.setComboWater("combo 1");

    }

    public void BtnBook_onClick(View v) {
        BookingAPI bookingAPI = APIClient.getClient().create(BookingAPI.class);
        bookingAPI.booking(booking).enqueue(new Callback<Booking>() {
            @Override
            public void onResponse(Call<Booking> call, Response<Booking> response) {
                try {
                    System.out.println("Status code: " + response.code());
                    System.out.println("Is success: --------------------" + response.isSuccessful());
                    Intent integer = new Intent(BookingActivity.this, MainActivity.class);
                    startActivity(integer);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Booking> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
