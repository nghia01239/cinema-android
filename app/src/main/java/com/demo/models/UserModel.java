package com.demo.models;

import com.demo.entities.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserModel implements Serializable {

    List<User> users = new ArrayList<User>();

    public List<User> anonymous(){
        users.add(new User("0", "username", "password", "R.drawable.anonymous", "Anonymous", "null", "gender", "null", "null", "null", "null"));
        return users;
    }
    public List<User> addUser( String username, String password, String avatar, String fullname, String birthday, String gender, String email, String phone, String city, String point){
        users.add(new User("1", username, password, avatar, fullname, birthday, gender, email, phone, city, point));
        return users;
    }


    public User findById(String id){
        for (User u : users){
            if(u.getId().equalsIgnoreCase(id)){
                return u;
            }
        }
        return null;
    }


    public User deleteUser(){
        users.remove(findById("1"));
        return findById("0");
    }

}
