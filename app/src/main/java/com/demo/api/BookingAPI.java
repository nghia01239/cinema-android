package com.demo.api;

import com.demo.entities.Booking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BookingAPI {

    @GET("booking/findBySchedule/{schedule_id}")
    Call<List<Booking>> findBySchedule(@Path("schedule_id") int schedule_id);

    @GET("booking/findByUser/{user_id}")
    Call<List<Booking>> findByUser(@Path("user_id") int user_id);

    @POST("booking/create_booking")
    Call<Booking> booking(@Body Booking booking);
}
