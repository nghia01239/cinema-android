package com.demo.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;
    public static Retrofit getClient() {



        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        client.connectTimeoutMillis();
        retrofit = new Retrofit.Builder().baseUrl("http://192.168.56.1:9596/api/")
                .addConverterFactory(GsonConverterFactory.create(gson)).client(client).build();
        return retrofit;
    }
}
