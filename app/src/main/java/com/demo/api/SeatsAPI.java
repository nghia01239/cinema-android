package com.demo.api;

import com.demo.entities.Seat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SeatsAPI {

    @GET("seat/findByRoom/{room_id}")
    Call<List<Seat>> findByRoom(@Path("room_id") int room_id);
}
