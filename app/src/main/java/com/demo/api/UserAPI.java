package com.demo.api;

import com.demo.entities.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserAPI {
    @GET("user/findall")
    Call<List<User>> findAll();
}
