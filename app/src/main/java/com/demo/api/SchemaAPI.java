package com.demo.api;

import com.demo.entities.Schedule;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SchemaAPI {

    @GET("schedule/findByMovie/{movie_id}/{n}/{now}")
    Call<List<Schedule>> findByMovie(@Path("movie_id") int movie_id, @Path("n") int n, @Path("now") String now);
}