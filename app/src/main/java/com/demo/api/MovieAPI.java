package com.demo.api;

import com.demo.entities.Movie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieAPI {

    @GET("movie/findall")
    Call<List<Movie>> findAll();
}
