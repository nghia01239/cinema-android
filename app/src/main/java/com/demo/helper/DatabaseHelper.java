package com.demo.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.demo.entities.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String dbName = "cinema2";
    private static int dbVersion = 1;

    private static String accountTable = "account";
    private static String idColumn = "id";
    private static String nameColumn = "name";
    private static String passColumn = "password";
    private static String avatarColumn = "avatar";
    private static String fullnameColumn = "fullname";
    private static String birthdayColumn = "birthday";
    private static String genderColumn = "gender";
    private static String emailColumn = "email";
    private static String phoneColumn = "phone";
    private static String cityColumn = "city";
    private static String pointColumn = "point";

    public DatabaseHelper( Context context) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + accountTable + "(" +
                        idColumn + " integer primary key , " +
                        nameColumn + " text, " +
                        passColumn + " datetime, " +
                        avatarColumn + " text, " +
                        fullnameColumn + " text, " +
                        birthdayColumn + " text, " +
                        genderColumn + " text, " +
                        emailColumn + " text, " +
                        phoneColumn + " text, " +
                        cityColumn + " text, " +
                        pointColumn + " text " +

               ")" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean creater(User user){
        try {

            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(idColumn, user.getId());
            contentValues.put(nameColumn, user.getUsername());
            contentValues.put(passColumn, user.getPassword());
            contentValues.put(avatarColumn, user.getAvatar());
            contentValues.put(fullnameColumn, user.getFullname());
            contentValues.put(birthdayColumn, user.getBirthday());
            contentValues.put(genderColumn, user.getGender());
            contentValues.put(emailColumn, user.getEmail());
            contentValues.put(phoneColumn, user.getPhone());
            contentValues.put(cityColumn, user.getCity());
            contentValues.put(pointColumn, user.getPoint());

            return sqLiteDatabase.insert(accountTable, null, contentValues) > 0;
        }catch (Exception e){
            e.printStackTrace();

            return false;
        }
    }
    public boolean delete(String id){
        try{
            SQLiteDatabase sqLiteDatabase= getWritableDatabase();
            return sqLiteDatabase.delete(accountTable, idColumn + " = ?",
                    new String[]{id}) >0;
        }catch (Exception e){
            return false;
        }
    }
    public User findAccount(){
        try {
            User user = null;
            SQLiteDatabase sqLiteDatabase= getWritableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + accountTable + " where "+ idColumn + " != 0", null);

            if(cursor.moveToFirst()){
                user = new User();
                do{

                        user.setId(cursor.getString(0));
                        user.setUsername(cursor.getString(1));
                        user.setPassword(cursor.getString(2));
                        user.setAvatar(cursor.getString(3));
                        user.setFullname(cursor.getString(4));
                        user.setBirthday(cursor.getString(5));
                        user.setGender(cursor.getString(6));
                        user.setEmail(cursor.getString(7));
                        user.setPhone(cursor.getString(8));
                        user.setCity(cursor.getString(9));
                        user.setPoint(cursor.getString(10));

                }while (cursor.moveToNext());
            }
            return user;
        }catch (Exception e){
            return null;
        }

    }
    public User findById(String id){
        try {
            User user = null;
            SQLiteDatabase sqLiteDatabase= getWritableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + accountTable + " where " + idColumn + " like ? "
                    , new  String[]{"%" + id + "%"});

            if(cursor.moveToFirst()){
                user = new User();
                do{
                    user.setId(cursor.getString(0));
                    user.setUsername(cursor.getString(1));
                    user.setPassword(cursor.getString(2));
                    user.setAvatar(cursor.getString(3));
                    user.setFullname(cursor.getString(4));
                    user.setBirthday(cursor.getString(5));
                    user.setGender(cursor.getString(6));
                    user.setEmail(cursor.getString(7));
                    user.setPhone(cursor.getString(8));
                    user.setCity(cursor.getString(9));
                    user.setPoint(cursor.getString(10));
                }while (cursor.moveToNext());
            }
            return user;
        }catch (Exception e){
            return null;
        }
    }

    public List<User> findAll(){
        try {
            List<User> users = null;
            SQLiteDatabase sqLiteDatabase= getWritableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + accountTable, null);

            if(cursor.moveToFirst()){
                users = new ArrayList<User>();
                User user = new User();
                do{

                        user.setId(cursor.getString(0));
                        user.setUsername(cursor.getString(1));
                        user.setPassword(cursor.getString(2));
                        user.setAvatar(cursor.getString(3));
                        user.setFullname(cursor.getString(4));
                        user.setBirthday(cursor.getString(5));
                        user.setGender(cursor.getString(6));
                        user.setEmail(cursor.getString(7));
                        user.setPhone(cursor.getString(8));
                        user.setCity(cursor.getString(9));
                        user.setPoint(cursor.getString(10));
                    users.add(user);
                }while (cursor.moveToNext());
            }
            return users;
        }catch (Exception e){
            return null;
        }

    }
}
