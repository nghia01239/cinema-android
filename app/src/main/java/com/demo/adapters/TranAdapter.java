package com.demo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.api.BookingAPI;
import com.demo.cinemaapplication.R;
import com.demo.entities.Booking;

import java.util.List;

public class TranAdapter extends ArrayAdapter<Booking> {
    private Context context;
    private int layout;
    private List<Booking> bookings;

    public TranAdapter(@NonNull Context context, int resource, @NonNull List<Booking> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.bookings = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(this.layout, null);
        TextView txtMovieName = view.findViewById(R.id.textViewTranMovieName);
        TextView txtCinemaName = view.findViewById(R.id.textViewTranCinemaName);
        TextView txtRoom = view.findViewById(R.id.textViewTranRoom);
        TextView txtSeat = view.findViewById(R.id.textViewTranSeat);
        TextView txtTime = view.findViewById(R.id.textViewTranTime);

        Booking booking= getItem(position);
        txtCinemaName.setText(booking.getSchedule().getRoom().getCinema().getCinemaName());
        txtMovieName.setText(booking.getSchedule().getMovie().getMovieName());
        txtRoom.setText(booking.getSchedule().getRoom().getRoomName());
        String seat = booking.getSeat().getRow() + " - " + booking.getSeat().getNumber();
        txtSeat.setText(seat);
        txtTime.setText(booking.getSchedule().getScheduleDate());

        return view;
    }
}
