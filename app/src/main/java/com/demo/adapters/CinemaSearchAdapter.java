package com.demo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.cinemaapplication.R;
import com.demo.entities.Schedule;

import java.util.ArrayList;
import java.util.List;

public class CinemaSearchAdapter extends ArrayAdapter<Schedule> {

    private Context context;
    private int layout;
    private List<Schedule> schedules = new ArrayList<Schedule>();
    private List<Schedule> filterSchedule = new ArrayList<Schedule>();

    public CinemaSearchAdapter(@NonNull Context context, int resource, @NonNull List<Schedule> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.schedules = objects;
    }

    @Override
    public int getCount() {
        return filterSchedule.size();
    }

    @Nullable
    @Override
    public Schedule getItem(int position) {
        return filterSchedule.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new ScheduleFilter(this, schedules);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(this.layout, null);
        TextView txtCinemaName = view.findViewById(R.id.textViewautoNameCinema);
        TextView txtCinemaAdd = view.findViewById(R.id.textViewautoAddressCinema);

        Schedule schedule = filterSchedule.get(position);
        txtCinemaName.setText(schedule.getRoom().getCinema().getCinemaName());
        txtCinemaAdd.setText(schedule.getRoom().getCinema().getCinemaAddress());
        return view;

    }

    private class ScheduleFilter extends Filter{
        CinemaSearchAdapter cinemaSearchAdapter;
        List<Schedule> schedules;
        List<Schedule> filterschedules;

        public ScheduleFilter(CinemaSearchAdapter cinemaSearchAdapter1, List<Schedule> schedules){
            this.cinemaSearchAdapter = cinemaSearchAdapter1;
            this.schedules = schedules;
            this.filterschedules = new ArrayList<Schedule>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filterschedules.clear();
            FilterResults filterResults = new FilterResults();
            if(constraint == null || constraint.length() ==0){
                filterschedules.addAll(schedules);
            }else {
                for (Schedule s : schedules){
                    if(s.getRoom().getCinema().getCinemaName().toLowerCase()
                            .contains(constraint.toString().toLowerCase())){
                        filterschedules.add(s);
                    }
                }
            }

            filterResults.values = filterschedules;
            filterResults.count = filterschedules.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            cinemaSearchAdapter.filterSchedule.clear();
            cinemaSearchAdapter.filterSchedule.addAll((List<Schedule>) results.values);
            cinemaSearchAdapter.notifyDataSetChanged();
        }
    }
}
