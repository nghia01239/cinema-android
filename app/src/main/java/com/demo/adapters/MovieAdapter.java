package com.demo.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.cinemaapplication.R;
import com.demo.entities.Movie;

import java.io.InputStream;
import java.util.List;

public class MovieAdapter extends ArrayAdapter<Movie> {

    private Context context;
    private int layout;
    private List<Movie> movies;

    public MovieAdapter(@NonNull Context context, int resource, @NonNull List<Movie> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.movies = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(this.layout, null);
        TextView txtNameMovie = view.findViewById(R.id.textViewNameMovie);
        TextView txtCens = view.findViewById(R.id.textViewCens);
        TextView txtDes = view.findViewById(R.id.textViewDes);
        ImageView imageView = view.findViewById(R.id.imageViewMovie);

        Movie movie = movies.get(position);
        txtNameMovie.setText(movie.getMovieName());
        txtCens.setText(movie.getMovieCens());
        txtDes.setText(movie.getMovieDescription());

        new ImageRequesAsk(imageView).execute("http://192.168.56.1:9567/resources/uploads/images/"+ movie.getMoviePoster());

        return view;
    }

    private class ImageRequesAsk extends AsyncTask<String, Void, Bitmap>{

        private ImageView imageViewPhoto;

        public ImageRequesAsk(ImageView imageViewPhoto){
            this.imageViewPhoto = imageViewPhoto;
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                InputStream inputStream = new java.net.URL(params[0]).openStream();
                return BitmapFactory.decodeStream(inputStream);
            }catch (Exception e){
                return  null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
           imageViewPhoto.setImageBitmap(bitmap);
        }
    }
}
