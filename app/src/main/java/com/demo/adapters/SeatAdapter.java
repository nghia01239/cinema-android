package com.demo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.cinemaapplication.R;
import com.demo.entities.Seat;

import java.util.List;


public class SeatAdapter extends ArrayAdapter<Seat> {

    private Context context;
    private int layout;
    private List<Seat> seats;

    public SeatAdapter(@NonNull Context context, int resource, @NonNull List<Seat> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.seats = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(this.layout, null);
        ImageView imageView = view.findViewById(R.id.imageViewSeat);
        TextView textView = view.findViewById(R.id.textViewNumverSeat);

        Seat seat = seats.get(position);
        if(seat.getChecked()){
            imageView.setImageResource(R.drawable.seat_icon);
        }else{
            imageView.setImageResource(R.drawable.seat_checked);
        }
        String col = "";
        if(seat.getRow().equalsIgnoreCase("Row 1")){
            col = "A";
        }else if(seat.getRow().equalsIgnoreCase("Row 2")){
            col = "B";
        }else if(seat.getRow().equalsIgnoreCase("Row 3")) {
            col = "C";
        }else if(seat.getRow().equalsIgnoreCase("Row 4")){
            col = "D";
        }else if(seat.getRow().equalsIgnoreCase("Row 5")){
            col = "E";
        }

        textView.setText(col + " - " + seat.getNumber() );

        return view;
    }
}
