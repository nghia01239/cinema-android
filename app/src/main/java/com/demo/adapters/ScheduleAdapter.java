package com.demo.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.demo.cinemaapplication.R;
import com.demo.entities.Movie;
import com.demo.entities.Schedule;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ScheduleAdapter extends ArrayAdapter<Schedule> {

    private Context context;
    private int layout;
    private List<Schedule> schedules;

    public ScheduleAdapter(@NonNull Context context, int resource, @NonNull List<Schedule> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.schedules = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(this.layout, null);
        TextView txtNameCinema = view.findViewById(R.id.textViewCinemaDetail);
        TextView txtAddress = view.findViewById(R.id.textViewCinemaAddressDetail);
        TextView txtRoomName = view.findViewById(R.id.textViewRoomNameDetail);
        TextView txtScheDate = view.findViewById(R.id.textViewScheDateDetail);
        TextView txtScheStart = view.findViewById(R.id.textViewScheStartDetail);


        Schedule schedule = schedules.get(position);
        txtNameCinema.setText(schedule.getRoom().getCinema().getCinemaName());
        txtAddress.setText(schedule.getRoom().getCinema().getCinemaAddress());
        txtRoomName.setText(schedule.getRoom().getRoomName());
        txtScheDate.setText(schedule.getScheduleDate());
        txtScheStart.setText(schedule.getScheduleStart());

        return view;
    }

}
